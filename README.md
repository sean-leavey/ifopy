# Ifopy
(Experimental) CAD for interferometer layout.

## Requirements
 - Python 3.7
 - Python bindings for OpenSCAD: `pythonocc 0.18.1`: https://github.com/tpaviot/pythonocc-core
 - Numpy