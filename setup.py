from setuptools import setup, find_packages

with open("README.md") as readme_file:
    README = readme_file.read()

REQUIREMENTS = [
]

# extra dependencies
EXTRAS = {
    "dev": [
        "sphinx",
        "sphinx_rtd_theme",
        "numpydoc"
    ]
}

setup(
    name="ifopy",
    use_scm_version={
        "write_to": "ifopy/_version.py"
    },
    description="CAD for interferometer layout",
    long_description=README,
    author="Sean Leavey",
    author_email="sean.leavey@ligo.org",
    url="https://git.ligo.org/sean-leavey/ifopy",
    packages=find_packages(),
    install_requires=REQUIREMENTS,
    extras_require=EXTRAS,
    setup_requires=['setuptools_scm'],
    license="GPLv3",
    zip_safe=False,
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7"
    ]
)
