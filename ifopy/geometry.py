import numpy as np
from numpy.linalg import norm

class Vector(np.ndarray):
    """2D vector"""

    def __new__(cls, x=0, y=0, z=0):
        obj = np.array((x, y, z))

        # return view as Vector
        return obj.view(cls)

    @classmethod
    def unit_x(cls):
        return cls(1, 0, 0)

    @classmethod
    def unit_y(cls):
        return cls(0, 1, 0)

    @classmethod
    def unit_z(cls):
        return cls(0, 0, 1)

    @property
    def x(self):
        return float(self[0])

    @x.setter
    def x(self, x):
        self[0] = x

    @property
    def y(self):
        return float(self[1])

    @y.setter
    def y(self, y):
        self[1] = y

    @property
    def z(self):
        return float(self[2])

    @z.setter
    def z(self, z):
        self[2] = z

    @property
    def rx(self):
        return self.angle_between(self.unit_x())

    @property
    def ry(self):
        return self.angle_between(self.unit_y())

    @property
    def rz(self):
        return self.angle_between(self.unit_z())

    @classmethod
    def origin(cls):
        """Origin vector"""
        return cls(0, 0, 0)

    @property
    def length(self):
        return norm(self)

    def unit(self):
        """Unit vector"""
        return self / self.length

    def distance_to(self, other):
        return norm(other - self)

    def angle_between(self, other):
        return np.dot(self, other) / (self.length * other.length)