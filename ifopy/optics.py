from OCC.BRepPrimAPI import BRepPrimAPI_MakeCylinder
from OCC.gp import gp_Pnt, gp_Vec, gp_Ax2, gp_Dir, gp_Trsf, gp_OX, gp_OY, gp_OZ
from OCC.Graphic3d import Graphic3d_NOM_GLASS
from OCC.BRepBuilderAPI import BRepBuilderAPI_Transform

from .geometry import Vector

class Mirror:
    def __init__(self, radius, thickness, material=None, transparency=0.5):
        if material is None:
            material = Graphic3d_NOM_GLASS

        self.radius = radius
        self.thickness = thickness
        self.material = material
        self.transparency = transparency

        # Default properties.
        self.pos = Vector(0, 0, 0)
        self.nv = Vector(1, 0, 0)
        self._shape = None

    @property
    def x(self):
        return self.pos.x

    @x.setter
    def x(self, x):
        self.pos.x = x

    @property
    def y(self):
        return self.pos.y

    @y.setter
    def y(self, y):
        self.pos.y = y

    @property
    def z(self):
        return self.pos.z

    @z.setter
    def z(self, z):
        self.pos.z = z

    @property
    def rx(self):
        return self.nv.rx

    @property
    def ry(self):
        return self.nv.ry

    @property
    def rz(self):
        return self.nv.rz

    @property
    def shape(self):
        # Make 3D object.
        self._make_shape()

        return self._shape.Shape()
    
    @property
    def _transformation(self):
        # Transformation vector.
        vector = gp_Vec(self.x, self.y, self.z)

        translation = gp_Trsf()
        translation.SetTranslation(vector)
        
        rx = gp_Trsf()
        rx.SetRotation(gp_OX(), self.rx)

        ry = gp_Trsf()
        ry.SetRotation(gp_OY(), self.ry)

        rz = gp_Trsf()
        rz.SetRotation(gp_OZ(), self.rz)

        return rx * ry * rz * translation

    def _make_shape(self):
        axe = gp_Ax2()
        #axe.SetLocation(gp_Pnt((random_vec()*scope).XYZ()))
        #axe.SetDirection(gp_Dir(random_vec()))
        origin_shape = BRepPrimAPI_MakeCylinder(axe, self.radius, self.thickness)

        # translate and rotate to final position
        self._shape = BRepBuilderAPI_Transform(origin_shape.Shape(), self._transformation, False)
        self._shape.Build()