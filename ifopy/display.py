from OCC.Display.SimpleGui import init_display


class Display:
    def __init__(self, model):
        self.model = model
        self._display = None

    def _add_shapes(self):
        for shape in self.model.shapes:
            self._display.DisplayShape(shape.shape, material=shape.material,
                                       transparency=shape.transparency)

    def show(self):
        display, start_display, __, __ = init_display()

        self._display = display
        self._add_shapes()

        start_display()