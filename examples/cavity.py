from ifopy.geometry import Vector
from ifopy.optics import Mirror
from ifopy.model import Model
from ifopy.display import Display

model = Model()

itm = Mirror(10, 10)
etm = Mirror(10, 10)

itm.x = 0
itm.y = 0
itm.z = 0
itm.nv = Vector(1, 0, 0)

etm.x = 100
etm.y = 0
etm.z = 0
itm.nv = Vector(-1, 0, 0)

model.shapes.append(itm)
model.shapes.append(etm)

display = Display(model)
display.show()